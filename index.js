console.log("Hi, B248!");
console.log(document);//result - html document
console.log(document.querySelector("#txt-first-name"));
console.log(document.querySelector("#txt-last-name"));
/*
	document - refers to the whole web page
	querySelector - used to select specific element (obj) as long as it is inside the html tag (HTML ELEMENT)
	-takes a string input that is formatted like CSS Selector
	-can select element regardless if the string is an id, class, or a tag as long as the element is existing in the webpage
*/
/*
	Alternative methods that we use aside from querySelector in retrieving elements

	document.getElementById()
	document.getElementByClassName()
	document.getElementByTagName()
*/


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


txtFirstName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtFirstName.value

})


/*
txtLastName.addEventListener('keyup',(event)=>{
	spanFullName.innerHTML = txtLastName.value

})
*/


txtFirstName.addEventListener('keyup',(event)=>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value)
})

// Additional Example

const labelFirstName = document.querySelector("#label-txt-first-name");

labelFirstName.addEventListener('click',(e)=>{

	alert("You cliked the First Name Label!")
})

labelFirstName.addEventListener('mouseover',(ev)=>{

	alert("You hovered the First Name Label!")
})

